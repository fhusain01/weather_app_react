import React, { useEffect, useState } from "react";
import axios from "axios";
import "./App.css";

function App() {
  const [weather, setWeather] = useState(null);
  const [city, setCity] = useState("bareilly");
  const [newCity, setNewCity] = useState("");
  useEffect(() => {
    axios
      .get(
        `http://api.weatherapi.com/v1/current.json?key=ee0b1d7d9ae24df69d0185509212708&q=${city}&aqi=yes`
      )
      .then((response) => {
        setWeather(response.data);
      });
  }, [city]);

  const updateCity = (e) => {
    setNewCity(e.target.value);
  };

  const updateWeather = () => {
    setCity(newCity);
    setNewCity("");
  };

  return (
    <div>
      <div className="input-field">
        <input type="text" onChange={updateCity} value={newCity} />
        <button onClick={updateWeather}>Submit</button>
      </div>
      <div>
        {weather && (
          <div className="weather-info">
            <h2>City: {weather.location.name}</h2>
            <h4>Temperature: {weather.current.temp_c} C</h4>
            <h4>Type: {weather.current.condition.text}</h4>
          </div>
        )}
      </div>
    </div>
  );
}

export default App;

// {
//   /* <div className="weather-info">
//   <h2>Country: {weather.location.country}</h2>
//   <h3>Temperature: weath.location.temp_c</h3>
// </div>; */
// }
